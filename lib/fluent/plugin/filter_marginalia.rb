# frozen_string_literal: true

require 'fluent/plugin/filter'

module Fluent
  module Plugin
    # Filters SQL statements for Marginalia comments.
    #
    # Examples:
    # SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/
    # /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/ SELECT COUNT(*) FROM "projects"
    #
    class Marginalia < Filter
      Fluent::Plugin.register_filter('marginalia', self)

      desc 'Field to parse for Marginalia comments (key1:value1,key2:value2)'
      config_param :key, :string, default: 'sql'

      desc 'Whether to strip the comment from the record specified by key'
      config_param :strip_comment, :bool, default: true

      MARGINALIA_PREPENDED_REGEXP = %r{^(?<comment>/\*.*\*/)(?<sql>.*)}m.freeze
      MARGINALIA_APPENDED_REGEXP = %r{(?<sql>.*)(?<comment>/\*.*\*/)$}m.freeze

      def filter(_tag, _time, record)
        parse_comments(record)

        record
      end

      private

      def parse_comments(record)
        sql = record[@key]

        return unless sql

        comment_match = match_marginalia_comment(sql)

        return unless comment_match

        entries = extract_entries(comment_match['comment'])
        parse_entries(entries, record)

        record[@key] = comment_match['sql'].strip if @strip_comment
      end

      def match_marginalia_comment(sql)
        matched = MARGINALIA_PREPENDED_REGEXP.match(sql)

        return matched if matched

        MARGINALIA_APPENDED_REGEXP.match(sql)
      end

      def extract_entries(comment)
        comment = scrub_comment(comment)

        return [] unless comment

        comment.split(',')
      end

      def scrub_comment(comment)
        return unless comment

        comment.strip!
        comment.gsub!(%r{^/\*}, '')
        comment.gsub!(%r{\*/$}, '')
      end

      def parse_entries(entries, record)
        entries.each do |component|
          data = component.split(':', 2)

          break unless data.length == 2

          stored_key = store_key(record, data[0])
          record[stored_key] = data[1]
        end
      end

      def store_key(record, component_key)
        # In case there is a conflict with the Marginalia key
        # (e.g. `correlation_id`), we use the base key
        # (`sql_correlation_id`) instead.
        if record.key?(component_key)
          "#{@key}_#{component_key}"
        else
          component_key
        end
      end
    end
  end
end
