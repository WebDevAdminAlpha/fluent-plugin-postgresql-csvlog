# frozen_string_literal: true

require_relative '../helper'

class PostgreSQLSlowLogTest < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
    @tag = 'test.tag'
  end

  CONFIG = '
    <filter test.tag>
      @type postgresql_slowlog
    </filter>
  '

  def create_driver(conf = CONFIG)
    Fluent::Test::Driver::Filter.new(Fluent::Plugin::PostgreSQLSlowLog).configure(conf)
  end

  test 'filters basic slow log' do
    d = create_driver

    inputs = [
      { 'message' => 'duration: 2357.1 ms  execute <unnamed>: SELECT * FROM projects' },
      { 'message' => 'duration: 1873.345 ms  execute <unnamed>: SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/' }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(inputs[0].merge(
                   {
                     'query' => 'SELECT * FROM projects',
                     'duration_s' => 2.3571
                   }
                 ),
                 d.filtered[0].last)
    assert_equal(inputs[1].merge(
                   {
                     'query' => 'SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/',
                     'duration_s' => 1.873345
                   }
                 ),
                 d.filtered[1].last)

    assert_equal(%w[duration_s query], d.filtered[0].last.keys.sort)
    assert_equal(%w[duration_s query], d.filtered[1].last.keys.sort)
  end

  test 'ignores messages not having to do with slow logs' do
    d = create_driver
    input = { 'message' => 'this is a test' }

    d.run(default_tag: @tag) do
      d.feed(input)
    end

    assert_equal(input, d.filtered[0].last)
    assert_equal(%w[message], d.filtered[0].last.keys.sort)
  end

  test 'outputs slow log entries to configured output key' do
    d = create_driver(
      <<~CONF
        output_key my_key
      CONF
    )

    inputs = [
      { 'message' => 'duration: 2357.1 ms  execute <unnamed>: SELECT * FROM projects' }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(inputs[0].merge(
                   {
                     'my_key' => 'SELECT * FROM projects',
                     'duration_s' => 2.3571
                   }
                 ),
                 d.filtered[0].last)

    assert_equal(%w[duration_s my_key], d.filtered[0].last.keys.sort)
  end
end
