# frozen_string_literal: true

require_relative '../helper'

class Marginalia < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
    @tag = 'test.tag'
  end

  CONFIG = '
    key statement
  '

  def create_driver(conf = CONFIG)
    Fluent::Test::Driver::Filter.new(Fluent::Plugin::Marginalia).configure(conf)
  end

  test 'parses appended Marginalia comments' do
    d = create_driver

    inputs = [
      { 'statement' => 'SELECT * FROM projects' },
      { 'statement' => 'SELECT COUNT(*) FROM "projects" /* this is just a comment */' },
      { 'statement' => 'SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/' },
      { 'statement' => 'SELECT COUNT(*) FROM "projects" /*application:web,correlation_id:01F1D2T1SC9DM82A4865ATG1CP,endpoint_id:POST /api/:version/groups/:id/-/packages/mavenpath/:file_name*/' }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(inputs[0], d.filtered[0].last)
    assert_equal(inputs[1], d.filtered[1].last)
    assert_equal(inputs[2].merge(
                   {
                     'application' => 'sidekiq',
                     'correlation_id' => 'd67cae54c169e0cab7d73389e2934f0e',
                     'jid' => '52a1c8a9e4c555ea573f20f0',
                     'job_class' => 'Geo::MetricsUpdateWorker'
                   }
                 ),
                 d.filtered[2].last)
    assert_equal(inputs[3].merge(
                   {
                     'application' => 'web',
                     'correlation_id' => '01F1D2T1SC9DM82A4865ATG1CP',
                     'endpoint_id' => 'POST /api/:version/groups/:id/-/packages/mavenpath/:file_name'
                   }
                 ),
                 d.filtered[3].last)

    assert_equal('SELECT * FROM projects', d.filtered[0].last['statement'])
    assert_equal('SELECT COUNT(*) FROM "projects"', d.filtered[1].last['statement'])
    assert_equal('SELECT COUNT(*) FROM "projects"', d.filtered[2].last['statement'])
    assert_equal('SELECT COUNT(*) FROM "projects"', d.filtered[3].last['statement'])
  end

  test 'parses prepended Marginalia comments' do
    d = create_driver

    inputs = [
      { 'statement' => '/* this is just a comment */ SELECT COUNT(*) FROM "projects"' },
      { 'statement' => '/*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/ SELECT COUNT(*) FROM "projects"' },
      { 'statement' => '/*application:web,correlation_id:01F1D2T1SC9DM82A4865ATG1CP,endpoint_id:POST /api/:version/groups/:id/-/packages/mavenpath/:file_name*/ SELECT COUNT(*) FROM "projects"' },
      { 'statement' => '/*application:sidekiq*/ SELECT COUNT(*) FROM "projects"',
        'application' => 'test-conflict' }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(inputs[0], d.filtered[0].last)
    assert_equal(inputs[1].merge(
                   {
                     'application' => 'sidekiq',
                     'correlation_id' => 'd67cae54c169e0cab7d73389e2934f0e',
                     'jid' => '52a1c8a9e4c555ea573f20f0',
                     'job_class' => 'Geo::MetricsUpdateWorker'
                   }
                 ),
                 d.filtered[1].last)
    assert_equal(inputs[2].merge(
                   {
                     'application' => 'web',
                     'correlation_id' => '01F1D2T1SC9DM82A4865ATG1CP',
                     'endpoint_id' => 'POST /api/:version/groups/:id/-/packages/mavenpath/:file_name'
                   }
                 ),
                 d.filtered[2].last)
    assert_equal(inputs[3].merge(
                   {
                     'statement_application' => 'sidekiq'
                   }
                 ),
                 d.filtered[3].last)

    assert_equal('SELECT COUNT(*) FROM "projects"', d.filtered[0].last['statement'])
    assert_equal('SELECT COUNT(*) FROM "projects"', d.filtered[1].last['statement'])
    assert_equal('SELECT COUNT(*) FROM "projects"', d.filtered[2].last['statement'])
  end

  test 'parses Marginalia comments with strip_comment disabled' do
    d = create_driver(
      <<~CONF
        strip_comment false
        key sql
      CONF
    )

    sql = %(SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/)
    appended_sql = %(SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/')

    inputs = [
      { 'sql' => sql },
      { 'sql' => appended_sql }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(sql, d.filtered[0].last['sql'])
    assert_equal(appended_sql, d.filtered[1].last['sql'])
  end
end
