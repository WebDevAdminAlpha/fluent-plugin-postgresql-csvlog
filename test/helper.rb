# frozen_string_literal: true

require 'bundler/setup'
require 'test/unit'

$LOAD_PATH.unshift(File.join(__dir__, '..', 'lib'))
$LOAD_PATH.unshift(__dir__)
require 'fluent/test'
require 'fluent/test/driver/filter'
require 'fluent/test/helpers'

Test::Unit::TestCase.include(Fluent::Test::Helpers)

require 'fluent/plugin/filter_postgresql_slowlog'
require 'fluent/plugin/filter_postgresql_redactor'
require 'fluent/plugin/filter_marginalia'
