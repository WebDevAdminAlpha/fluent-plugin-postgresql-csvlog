# frozen_string_literal: true

require 'fluent/plugin/filter'
require 'pg_query'

module Fluent::Plugin
  class PostgreSQLRedactor < Filter
    Fluent::Plugin.register_filter('postgresql_redactor', self)

    desc 'Input field to parse for SQL queries'
    config_param :input_key, :string, default: 'query'

    desc 'Output field to store SQL queries'
    config_param :output_key, :string, default: 'sql'

    desc 'Name of field to store SQL query fingerprint'
    config_param :fingerprint_key, :string, default: 'fingerprint'

    def filter(_tag, _time, record)
      statement = record[@input_key]

      return record unless statement

      normalized = PgQuery.normalize(statement)
      record[@fingerprint_key] = PgQuery.parse(normalized).fingerprint if @fingerprint_key

      record.delete(@input_key)
      record[@output_key] = normalized

      record
    rescue PgQuery::ParseError
      record['pg_query_error'] = true
      record
    end
  end
end
